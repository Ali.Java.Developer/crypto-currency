package com.example.coin.cryptoCurrency;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashSet;

@Service
public class CryptoCurrencyCrawlerService {

    @Autowired
    private CryptoCurrencyService cryptoCurrencyService;

    private final String baseURL = "https://arzdigital.com/coins/";
    private Document cryptoCurrencyDocument;
    private HashSet<String> pageLinks;
    private static final int DEPTH = 20;

    @Scheduled(fixedRate = 10 * 59 * 1000)
    public void cryptoCurrencyDataMiningAllPageEveryTenMinutes() {
        pageLinks = new HashSet<String>();
        int depth = 0;
        cryptoCurrencyDataMiningAllPage(baseURL, depth);
    }

    public void cryptoCurrencyDataMiningAllPage(String pageURL, int depth) {
        if ((!pageLinks.contains(pageURL)) && (depth <= 20)) {
            System.out.println(">> Depth: " + depth + " [ " + pageURL + " ]");
            try {
                pageLinks.add(pageURL);

                cryptoCurrencyDocument = Jsoup.connect(pageURL).get();
                Elements linksOnPage = cryptoCurrencyDocument.select("ul.arz-coin-pagination__list li a[class]");
                saveCryptoCurrency();
                depth++;
                for (Element page : linksOnPage) {
                    cryptoCurrencyDataMiningAllPage(page.attr("abs:href"), depth);
                }
            } catch (IOException e) {
                System.err.println("For '" + pageURL + "': " + e.getMessage());
            }
        }
    }

    public CryptoCurrency saveCryptoCurrency() {
        CryptoCurrency result = null;
        try {
            Elements nameOfCryptoCurrencies = nameOfCryptoCurrencyDataMining();
            Elements dollarPriceOfCryptoCurrencies = dollarPriceOfCryptoCurrencyDataMining();
            Elements tomanPriceOfCryptoCurrencies = tomanPriceOfCryptoCurrencyDataMining();
            for (int i = 0; i < nameOfCryptoCurrencies.size(); i++) {
                String nameOfCryptoCurrency = nameOfCryptoCurrencies.get(i).text();
                BigDecimal dollarPriceOfCryptoCurrency = convertDollarElementToBigDecimal(dollarPriceOfCryptoCurrencies.get(i));
                BigDecimal tomanPriceOfCryptoCurrency = formatNumbersElementToBigDecimal(tomanPriceOfCryptoCurrencies.get(i));
                CryptoCurrency cryptoCurrency = new CryptoCurrency(nameOfCryptoCurrency, dollarPriceOfCryptoCurrency, tomanPriceOfCryptoCurrency);
                try {
                    result = cryptoCurrencyService.saveNewCryptoCurrency(cryptoCurrency);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Elements nameOfCryptoCurrencyDataMining() {
        Elements cryptoCurrenciesName = cryptoCurrencyDocument.select("tr > td a");
        return cryptoCurrenciesName;
    }

    public Elements dollarPriceOfCryptoCurrencyDataMining() {
        Elements cryptoCurrencyDollarPrice = cryptoCurrencyDocument.select("tr > td > span.pulser");
        return cryptoCurrencyDollarPrice;
    }

    public Elements tomanPriceOfCryptoCurrencyDataMining() {
        Elements cryptoCurrencyTomanPrice = cryptoCurrencyDocument.select("tr > td > span span[class]:first-child");
        return cryptoCurrencyTomanPrice;
    }

    public BigDecimal formatNumbersElementToBigDecimal(Element cryptoCurrencyPrice) {
        String priceOfCryptoCurrencyString = cryptoCurrencyPrice.text();
        String[] arrayPriceStrings = priceOfCryptoCurrencyString.split(",");
        String totalPriceString = "";
        for (String item : arrayPriceStrings) {
            totalPriceString += item;
        }
        BigDecimal cryptocurrencyPriceBigDecimal = new BigDecimal(totalPriceString);
        return cryptocurrencyPriceBigDecimal;
    }

    public BigDecimal convertDollarElementToBigDecimal(Element cryptoCurrencyDollarPrice) {
        String dollarPriceOfCryptoCurrencyString = cryptoCurrencyDollarPrice.text().substring(1);
        //todo REFACTOR
        String[] arrayPriceStrings = dollarPriceOfCryptoCurrencyString.split(",");
        String totalPriceString = "";
        for (String item : arrayPriceStrings) {
            totalPriceString += item;
        }
        BigDecimal dollarPriceOfCryptoCurrency = new BigDecimal(totalPriceString);
        return dollarPriceOfCryptoCurrency;
    }
}
