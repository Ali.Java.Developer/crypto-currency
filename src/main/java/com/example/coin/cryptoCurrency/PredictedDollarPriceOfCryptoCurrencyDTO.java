package com.example.coin.cryptoCurrency;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PredictedDollarPriceOfCryptoCurrencyDTO {
    private double predictedDollarPrice;
    private String time;
}
