package com.example.coin.cryptoCurrency;

import com.example.coin.linearRegression.LinearRegression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.List;

@Service
public class CryptoCurrencyService {

    @Autowired
    private CryptoCurrencyRepository cryptoCurrencyRepository;

    public CryptoCurrency saveNewCryptoCurrency(CryptoCurrency cryptoCurrency) throws NullPointerException {
        CryptoCurrency savedCryptoCurrency = cryptoCurrencyRepository.save(cryptoCurrency);
        if (cryptoCurrency == null) {
            throw new NullPointerException();
        }
        return savedCryptoCurrency;
    }

    public List<CryptoCurrency> findAll() {
        List<CryptoCurrency> foundAll = cryptoCurrencyRepository.findAll();
        return foundAll;
    }

    public List<CryptoCurrency> findCryptoCurrencyByName(String coinType) {
        List<CryptoCurrency> foundCryptoCurrency = cryptoCurrencyRepository.findCryptoCurrencyByName(coinType);
        return foundCryptoCurrency;
    }


    public long stringTimeToLong(String timeString) {
        Long result = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.parse(timeString, formatter);
        result = Timestamp.valueOf(localDateTime).getTime();
        return result;
    }

    public List<CryptoCurrency> findLatestCryptoCurrencyPrice() {
        List<CryptoCurrency> result = null;
        Timestamp currentTime = new Timestamp(System.currentTimeMillis());
        long tenMinuteAgo = currentTime.getTime() - (10 * 59 * 1000);
        result = cryptoCurrencyRepository.findLatestCryptoCurrencyByTime(tenMinuteAgo);
        //todo add exception
        return result;
    }
    public double[] getDollarsPrice(String cryptoCurrencyName){
        List<CryptoCurrency> foundCryptoCurrency = cryptoCurrencyRepository.findCryptoCurrencyByName(cryptoCurrencyName);
        int sizeOfCryptoCurrency = foundCryptoCurrency.size();
        double[] dollarPrices = new double[sizeOfCryptoCurrency];
        for (int i = 0 ; i < sizeOfCryptoCurrency ; i++) {
            dollarPrices[i] = foundCryptoCurrency.get(i).getDollarPrice().doubleValue();
        }
        return dollarPrices;
    }
    public double[] getCryptoCurrencyTimes(String cryptoCurrencyName){
        List<CryptoCurrency> foundCryptoCurrency = cryptoCurrencyRepository.findCryptoCurrencyByName(cryptoCurrencyName);
        int sizeOfCryptoCurrency = foundCryptoCurrency.size();
        double[] times = new double[sizeOfCryptoCurrency];
        for (int i = 0 ; i < sizeOfCryptoCurrency ; i++) {
            times[i] = foundCryptoCurrency.get(i).getTime();
        }
        return times;
    }


}




