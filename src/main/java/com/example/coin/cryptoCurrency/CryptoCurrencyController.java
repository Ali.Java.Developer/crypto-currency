package com.example.coin.cryptoCurrency;

import com.example.coin.linearRegression.LinearRegression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class CryptoCurrencyController {

    @Autowired
    private CryptoCurrencyService cryptoCurrencyService;

    @GetMapping("/all")
    public List<CryptoCurrency> findAllCryptoCurrency() {
        List<CryptoCurrency> result = cryptoCurrencyService.findAll();
        return result;
    }

    @GetMapping("/all/{coinType}")
    public List<CryptoCurrency> findAllSpecificCoin(@PathVariable String coinType) {
        List<CryptoCurrency> result = cryptoCurrencyService.findCryptoCurrencyByName(coinType);
        return result;
    }

    @GetMapping("/coins")
    public List<CryptoCurrency> latestCoinsPrice() {
        List<CryptoCurrency> result = cryptoCurrencyService.findLatestCryptoCurrencyPrice();
        return result;
    }

    @PostMapping("/predict/{cryptoCurrency}")
    public PredictedDollarPriceOfCryptoCurrencyDTO predictDollarPriceOfCryptoCurrency(@PathVariable(value = "cryptoCurrency") String cryptoCurrency,@RequestParam(value = "time") String timeString){
        PredictedDollarPriceOfCryptoCurrencyDTO result = new PredictedDollarPriceOfCryptoCurrencyDTO();
        double[] dollarsPrice = cryptoCurrencyService.getDollarsPrice(cryptoCurrency);
        double[] times = cryptoCurrencyService.getCryptoCurrencyTimes(cryptoCurrency);
        LinearRegression linearRegression = new LinearRegression(times , dollarsPrice);
        double time = cryptoCurrencyService.stringTimeToLong(timeString);
        double dollarPredicted = linearRegression.predict(time);
        result.setPredictedDollarPrice(dollarPredicted);
        result.setTime(timeString);

        System.out.println("predict value : " + linearRegression.predict(time));

        return result;
    }



}
