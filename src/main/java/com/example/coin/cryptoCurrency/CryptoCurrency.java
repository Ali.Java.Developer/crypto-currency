package com.example.coin.cryptoCurrency;

import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
public class CryptoCurrency {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String name;

    @NotNull
    @Column(name = "dollar_price" , nullable = false)
    private BigDecimal dollarPrice;

    @NotNull
    @Column(name = "toman_price" , nullable = false)
    private BigDecimal tomanPrice;

    @NotNull
    @Column(nullable = false)
    private Long time;

    public CryptoCurrency(String name, BigDecimal dollarPrice, BigDecimal tomanPrice) {
        this.name = name;
        this.dollarPrice = dollarPrice;
        this.tomanPrice = tomanPrice;
    }

    public CryptoCurrency() {
    }

    @PrePersist
    private void preInsertTime(){
        Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());
        time = currentTimestamp.getTime();
    }


}
