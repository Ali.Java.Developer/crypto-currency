package com.example.coin.cryptoCurrency;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CryptoCurrencyRepository extends JpaRepository<CryptoCurrency , Long> {

    List<CryptoCurrency> findCryptoCurrencyByName(String name);

    @Query(nativeQuery = true , value = "select * from crypto_currency.crypto_currency where time > :tenMinuteAgo")
    List<CryptoCurrency> findLatestCryptoCurrencyByTime(@Param("tenMinuteAgo")long tenMinuteAgo);


}
